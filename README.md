Running Instructions:

1. In the root directory, run `pipenv install`. Activate your virtual environment, which is different command depending on your shell.
2. Then start the django server with `python manage.py runserver`. The API server should be running.
3. In a new terminal window, navigate to `sassville_library/books-fe` and run `npm i`, then `npm start`.

Notes:

There are two upsetting bugs, and one short-cut, left in this code. 

First, when a book is reserved, and the user is taken to the "Reserved Books" page, the most recently reserved book may not show up (though the API hit will have succeeded). The solution would be to wait for the "reserve book" functionality to resolve in a promise, before handling the redirect.

Second, the text input box used to search for books by title does not reliably reset itself after a search. The solution would be to, frankly, use an off-the-shelf SearchBox component rather than rolling out my own, but writing my own was the more timely solution at present.

Finally, implementing proper authentication would be outside the scope of this exercise, so only a single user is kept track of, and this is accomplished in the worst possible way: the user_id is hardcoded and all operations are performed against the user with primary key == 1.


Certainly not my best work, but likely good enough for a proof of concept.