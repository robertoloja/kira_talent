from django.db import models
from django.contrib.auth import get_user_model


class Book(models.Model):
    title = models.CharField(max_length=256)
    author = models.CharField(max_length=256)  # in a full project, this should be a ForeignKey to an Author's table
    reserved_by = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, null=True)  # NULL == not reserved

    def __str__(self):
        return self.title

    def available_copy(self):
        available_copies = Book.objects.filter(title=self.title, reserved_by=None)

        if available_copies.exists():
            return available_copies.first()

        else:  # unnecessary, but air on the side of explicitness
            return None

    def reserve(self, user):
        if not self.reserved_by:
            # I'm going to cheat this to avoid adding authentication to a proof of concept
            self.reserved_by_id = 1
            self.save()
            return True

        else:
            other_copy = self.available_copy()

            if other_copy:
                other_copy.reserve(user)
                return True

        return False
