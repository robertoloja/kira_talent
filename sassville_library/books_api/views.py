from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.contrib.auth.models import User

from .models import Book
from .serializers import BookSerializer


@api_view(['GET'])
def book_list(request):
    data = Book.objects.all()
    serializer = BookSerializer(data,
                                context={'request': request},
                                many=True)

    return Response(serializer.data)


@api_view(['GET'])
def reserved_books(request):
    data = Book.objects.filter(reserved_by_id=1)

    serializer = BookSerializer(data,
                                context={'request': request},
                                many=True)

    return Response(serializer.data)


@api_view(['GET'])
def reserve_book(request, book_id):
    book = Book.objects.get(id=book_id)
    book.reserve(request.user)

    return Response()


@api_view(['GET'])
def search_books(request, term):
    books = Book.objects.filter(title__icontains=term)
    serializer = BookSerializer(books,
                                context={'request': request},
                                many=True)

    return Response(serializer.data)
