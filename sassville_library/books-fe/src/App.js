import { useState } from 'react';
import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
} from "react-router-dom";
import Home from "./components/Home";
import ReservedBooks from "./components/ReservedBooks"
import SearchBooks from "./components/SearchBooks"

class App extends Component {
  render() {
    return (
        <Router>
          <div>
            <nav>
              <ul>
                <li key={1}>
                  <Link to="/">Home</Link>
                </li>
                <li key={2}>
                  <Link to="/reserved_books">Your Reserved Books</Link>
                </li>
                <li key={3}>
                  <SearchBox />
                </li>
              </ul>
            </nav>

            <Switch>
              <Route path="/reserved_books">
                <ReservedBooks />
              </Route>
              <Route path="/search_books/:term" component={SearchBooks} >
              </Route>
              <Route path="/">
                <Home />
              </Route>

            </Switch>
          </div>
        </Router>
    )
  }
}

const SearchBox = () => {
  let [input, setInput] = useState('');

  return (
    <div>
      <p>Find a book by title:
      <input value={input} onInput={e => setInput(e.target.value)}/>
      <Link to={"/search_books/" + input} onClick={() => setInput('')}>Search</Link></p>
    </div>
  );
}

export default App;