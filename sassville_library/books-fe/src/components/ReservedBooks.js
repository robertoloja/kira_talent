import axios from "axios";
import { API_URL } from "../constants";
import Home from "./Home";


class ReservedBooks extends Home {
  getBooks = () => {
    axios.get(API_URL + 'reserved_books/').then(res => this.setState({ books: res.data }));
  };
}

export default ReservedBooks;