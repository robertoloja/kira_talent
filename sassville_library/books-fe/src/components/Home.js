import React, { Component } from "react";
import BookList from "./BookList";

import axios from "axios";

import { API_URL } from "../constants";

class Home extends Component {
  state = {
    books: []
  };

  componentDidMount() {
    this.resetState();
  }

  getBooks = () => {
    axios.get(API_URL + 'books/').then(res => this.setState({ books: res.data }));
  };

  resetState = () => {
    this.getBooks();
  };

  render() {
    return (
          <div>
            <BookList
              books={this.state.books}
              resetState={this.resetState}
            />
          </div>
    );
  }
}

export default Home;