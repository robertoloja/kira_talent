import axios from "axios";
import { API_URL } from "../constants";
import Home from "./Home";


class SearchBooks extends Home {
  getBooks = () => {
    console.log(this.props)
    let term = this.props.match.params.term
    axios.get(API_URL + 'search_books/' + term).then(res => this.setState({ books: res.data }));
  };
}

export default SearchBooks;