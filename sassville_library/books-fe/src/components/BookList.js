import React, { Component } from "react";
import axios from "axios";
import { API_URL } from "../constants";
import {
  Link
} from "react-router-dom";


const Book = (props) => {
    const book = props.book;
    return (
      <tr key={book.id}>
        <td>{book.title}</td>
        <td>{book.author}</td>
        <td><Link to='reserved_books/' onClick={() => { reserveBook(book.id) }}>Reserve</Link></td>
      </tr>
    )
}

const reserveBook = (book_id) => {
  axios.get(API_URL + 'reserve_book/' + book_id).then();
}

class BookList extends Component {
  render() {
    const books = this.props.books;
    return (
      <table>
        <thead>
          <tr>
            <th>Title</th>
            <th>Author</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {!books || books.length <= 0 ? (
            <tr>
              <td colSpan="3" align="center">
                <b>Ops, no one here yet</b>
              </td>
            </tr>
          ) : (
            books.map(book => <Book book={book} />)
          )}
        </tbody>
      </table>
    );
  }
}

export default BookList;